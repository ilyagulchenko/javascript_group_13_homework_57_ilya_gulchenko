import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { User } from '../shared/user.model';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent {
  @Output() addUser = new EventEmitter<User>();
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  @ViewChild('isActive') isActive!: ElementRef;
  @ViewChild('roleInput') roleInput!: ElementRef;

  createUser(event: Event) {
    event.preventDefault();
    const name: string = this.nameInput.nativeElement.value;
    const email: string = this.emailInput.nativeElement.value;
    const active = this.isActive.nativeElement.checked;
    const role = this.roleInput.nativeElement.value;

    const user = new User(name, email, active, role);
    this.addUser.emit(user);
  }
}
