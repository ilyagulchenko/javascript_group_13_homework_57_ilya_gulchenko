import { Component } from '@angular/core';
import { User } from './shared/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  users: User[] = [

  ];

  onAddClick(user: User) {
    this.users.push(user);
    console.log(this.users);
  }
}
